$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("BuildingDetails.feature");
formatter.feature({
  "line": 1,
  "name": "Dashboard Tests",
  "description": "Check Dashboard functions, Check Widgets, Navigate through buildings, floors and devices",
  "id": "dashboard-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6916231498,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 2435376756,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2231302742,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 2047028826,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Go to one Floor from the list",
  "description": "",
  "id": "dashboard-tests;go-to-one-floor-from-the-list",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@SmokeTest"
    },
    {
      "line": 10,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "Clicks Basement from the list",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2126607651,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1024904325,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6181232528,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 38638805,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2191155301,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 2236855896,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Go to one Floor from the Floors button",
  "description": "",
  "id": "dashboard-tests;go-to-one-floor-from-the-floors-button",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 15,
      "name": "@SmokeTest"
    },
    {
      "line": 15,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "User Click on floors button",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "Check if list of floors appears",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "Clicks Basement from the list",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickFloors()"
});
formatter.result({
  "duration": 2172789637,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.openFloors()"
});
formatter.result({
  "duration": 1024819455,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2105679670,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1024428499,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5413549066,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 39737934,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2193909153,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 2189550665,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Go to one Device from the Devices button",
  "description": "",
  "id": "dashboard-tests;go-to-one-device-from-the-devices-button",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@SmokeTest"
    },
    {
      "line": 22,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "User Click on Devices button",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "Click at BR126 Device",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "Check if BR126 Device pop-up appears",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1183567134,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1029444610,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openAreasTab(String)"
});
formatter.result({
  "duration": 269858997,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMensuTsab(String)"
});
formatter.result({
  "duration": 1020662242,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6888003814,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 45066625,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2192102315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1964903916,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Add Widget on Dashboard",
  "description": "",
  "id": "dashboard-tests;add-widget-on-dashboard",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@SmokeTest"
    },
    {
      "line": 29,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "User Click on Edit Dashboard",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "Check if the Edit Dashboard page appears",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Click add for Navigation Widget",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "Click add for Summary Widget",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Click Save Dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Check if Navigation Widget appears on Dashboard",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "Check if Summary Widget appears on Dashboard",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickDashboard()"
});
formatter.result({
  "duration": 96974286,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.EditDashboard()"
});
formatter.result({
  "duration": 1023700848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Navigation",
      "offset": 14
    }
  ],
  "location": "DashboardStepDefs.openAreassTab(String)"
});
formatter.result({
  "duration": 126500984,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Summary",
      "offset": 14
    }
  ],
  "location": "DashboardStepDefs.openAreassTab(String)"
});
formatter.result({
  "duration": 153564611,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.SaveClick()"
});
formatter.result({
  "duration": 1115560705,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Navigation",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.checkWidgetAdded(String)"
});
formatter.result({
  "duration": 1026404613,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Summary",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.checkWidgetAdded(String)"
});
formatter.result({
  "duration": 1026064208,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6325427640,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34868373,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2193175473,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 2211894528,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "Edit Navigation Widget on Dashboard",
  "description": "",
  "id": "dashboard-tests;edit-navigation-widget-on-dashboard",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 39,
      "name": "@SmokeTest"
    },
    {
      "line": 39,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "User Click on Edit Dashboard",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "Click edit for Navigation Widget",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Add BR126 Device at Widget",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "Check if BR126 device appears on Navigation Widget",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickDashboard()"
});
formatter.result({
  "duration": 93884666,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Navigation",
      "offset": 15
    }
  ],
  "location": "DashboardStepDefs.openEditTab(String)"
});
formatter.result({
  "duration": 2239558735,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 4
    }
  ],
  "location": "DashboardStepDefs.openaddseTab(String)"
});
formatter.result({
  "duration": 1777284844,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.checkWidgetUpdated(String)"
});
formatter.result({
  "duration": 1548913115,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7673589172,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 43137815,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2180606905,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1949247583,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Edit Summary Widget on Dashboard",
  "description": "",
  "id": "dashboard-tests;edit-summary-widget-on-dashboard",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 46,
      "name": "@SmokeTest"
    },
    {
      "line": 46,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 48,
  "name": "User Click on Edit Dashboard",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "Click edit for Summary Widget",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "Add DEV277126~2dsystemStatus and BO1 points for BR126 Device at Summary Widget",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "Check if DEV277126~2dsystemStatus and BO1 points appears on Summary Widget",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickDashboard()"
});
formatter.result({
  "duration": 99083502,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Summary",
      "offset": 15
    }
  ],
  "location": "DashboardStepDefs.openEditTab(String)"
});
formatter.result({
  "duration": 2234398855,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DEV277126~2dsystemStatus",
      "offset": 4
    },
    {
      "val": "BO1",
      "offset": 33
    },
    {
      "val": "BR126",
      "offset": 48
    }
  ],
  "location": "DashboardStepDefs.openMenTab(String,String,String)"
});
formatter.result({
  "duration": 5739903314,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DEV277126~2dsystemStatus",
      "offset": 9
    },
    {
      "val": "BO1",
      "offset": 38
    }
  ],
  "location": "DashboardStepDefs.checkPoinsUpdated(String,String)"
});
formatter.result({
  "duration": 2026538642,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5567604343,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 64606549,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2205974070,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1933104294,
  "status": "passed"
});
formatter.scenario({
  "line": 54,
  "name": "Go to one Device from Summary Widget",
  "description": "",
  "id": "dashboard-tests;go-to-one-device-from-summary-widget",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 53,
      "name": "@SmokeTest"
    },
    {
      "line": 53,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 55,
  "name": "User Click BR126 from Summary Widget",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "Check if BR126 Device pop-up appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 11
    }
  ],
  "location": "DashboardStepDefs.ClickDeviceSumWidget(String)"
});
formatter.result({
  "duration": 1636980279,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMensuTsab(String)"
});
formatter.result({
  "duration": 1020553720,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7512799988,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35496779,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2186748580,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1891802071,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "Go to one Device from Navigation Widget",
  "description": "",
  "id": "dashboard-tests;go-to-one-device-from-navigation-widget",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 58,
      "name": "@SmokeTest"
    },
    {
      "line": 58,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 60,
  "name": "User Click BR126 from Navigation Widget",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "Check if BR126 Device pop-up appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 11
    }
  ],
  "location": "DashboardStepDefs.ClickDeviceNavWidget(String)"
});
formatter.result({
  "duration": 625227478,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMensuTsab(String)"
});
formatter.result({
  "duration": 1020652502,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6825869610,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35745358,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2217803392,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1905097363,
  "status": "passed"
});
formatter.scenario({
  "line": 64,
  "name": "Remove Widget from Dashboard",
  "description": "",
  "id": "dashboard-tests;remove-widget-from-dashboard",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 63,
      "name": "@SmokeTest"
    },
    {
      "line": 63,
      "name": "@Dashboard"
    }
  ]
});
formatter.step({
  "line": 65,
  "name": "User Click on Edit Dashboard",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "Click remove for Navigation Widget",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "Click remove for Summary Widget",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "Click Save Dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "Check if Navigation Widget disappears from Dashboard",
  "keyword": "Then "
});
formatter.step({
  "line": 70,
  "name": "Check if Summary Widget disappears from Dashboard",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickDashboard()"
});
formatter.result({
  "duration": 98220894,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Navigation",
      "offset": 17
    }
  ],
  "location": "DashboardStepDefs.openRemoveTab(String)"
});
formatter.result({
  "duration": 3125793275,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Summary",
      "offset": 17
    }
  ],
  "location": "DashboardStepDefs.openRemoveTab(String)"
});
formatter.result({
  "duration": 3105914801,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.SaveClick()"
});
formatter.result({
  "duration": 1058992641,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Navigation",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.checkWidgetRemoved(String)"
});
formatter.result({
  "duration": 1029452958,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Summary",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.checkWidgetRemoved(String)"
});
formatter.result({
  "duration": 1011876627,
  "status": "passed"
});
formatter.uri("BuildingPanel.feature");
formatter.feature({
  "line": 1,
  "name": "Building Panel Tests",
  "description": "",
  "id": "building-panel-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6780077653,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34545592,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 88722004,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 29011915,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Add New Building",
  "description": "",
  "id": "building-panel-tests;add-new-building",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@Admin"
    },
    {
      "line": 9,
      "name": "@SmokeTest"
    },
    {
      "line": 9,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "User clicks on Add new button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User add Auto Test Building as Name",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User add 277 Gratiot Ave as Address 1",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "User add Ste 100 as Address 2",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "User add 48127 as ZIP code",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User add Detroit as City",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User add Alabama as State",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User add USA as Country",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User add 42.3314 as Latitude",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User add -83.0458 as Longitude",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "User add 2009 as Year build",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User add 208 as Area",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User add test1.jpg as Image",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "Check if Auto Test Building appear on list",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.OpenClickAddBuildingButton()"
});
formatter.result({
  "duration": 269250533,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillName(String)"
});
formatter.result({
  "duration": 1461694644,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "277 Gratiot Ave",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillAdd1(String)"
});
formatter.result({
  "duration": 314659418,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ste 100",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillAdd2(String)"
});
formatter.result({
  "duration": 215619047,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "48127",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillZip(String)"
});
formatter.result({
  "duration": 161119850,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Detroit",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillCity(String)"
});
formatter.result({
  "duration": 219738231,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alabama",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillState(String)"
});
formatter.result({
  "duration": 223184025,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "USA",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillCountry(String)"
});
formatter.result({
  "duration": 160768778,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "42.3314",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillLat(String)"
});
formatter.result({
  "duration": 179995196,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-83.0458",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillLong(String)"
});
formatter.result({
  "duration": 192914372,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2009",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillYear(String)"
});
formatter.result({
  "duration": 131272690,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "208",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillArea(String)"
});
formatter.result({
  "duration": 125016928,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test1.jpg",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.FillImage(String)"
});
formatter.result({
  "duration": 3062914261,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1075302887,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.checkBuildingIsAdded(String)"
});
formatter.result({
  "duration": 3094520492,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5326340799,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34962054,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 74968046,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 41814686,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "Edit Building",
  "description": "",
  "id": "building-panel-tests;edit-building",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 27,
      "name": "@Admin"
    },
    {
      "line": 27,
      "name": "@SmokeTest"
    },
    {
      "line": 27,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 29,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Click on Edit Button, then change some values and click save",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "Check if All Changed fields are saved successful for Auto Test Building",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3054382790,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.editBuilding()"
});
formatter.result({
  "duration": 3244247890,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 53
    }
  ],
  "location": "WebsiteStepDefs.checkEditBuilding(String)"
});
formatter.result({
  "duration": 49408881,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5257635472,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34661069,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 73830888,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20991980,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Add New Floor",
  "description": "",
  "id": "building-panel-tests;add-new-floor",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 33,
      "name": "@Admin"
    },
    {
      "line": 33,
      "name": "@SmokeTest"
    },
    {
      "line": 33,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 35,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "User click on Areas Tab",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "User click Add Single floor button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "User Enter autoArea in name field",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "Check if floor autoArea appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3051825111,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openAreaTab()"
});
formatter.result({
  "duration": 640042073,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2068319936,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabAddSingleButton()"
});
formatter.result({
  "duration": 643483229,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.addfloorfield(String)"
});
formatter.result({
  "duration": 130825154,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1073187642,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2054681921,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea",
      "offset": 15
    }
  ],
  "location": "WebsiteStepDefs.CheckFloorAppears(String)"
});
formatter.result({
  "duration": 2107512944,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5288631848,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 49383375,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 93048494,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 21412154,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Add Multiple Floors",
  "description": "",
  "id": "building-panel-tests;add-multiple-floors",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 44,
      "name": "@Admin"
    },
    {
      "line": 44,
      "name": "@SmokeTest"
    },
    {
      "line": 44,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 46,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "User click on Areas Tab",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "User click Add Multiple Floors button",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "User Enter TEST in name field and Range from 1 to 3",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "Check if floors with name TEST 1 until TEST 3 appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3050922155,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openAreaTab()"
});
formatter.result({
  "duration": 615251835,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2100566631,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabAddMultipleButton()"
});
formatter.result({
  "duration": 685185220,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TEST",
      "offset": 11
    },
    {
      "val": "1",
      "offset": 45
    },
    {
      "val": "3",
      "offset": 50
    }
  ],
  "location": "WebsiteStepDefs.addMultiplefloorfield(String,String,String)"
});
formatter.result({
  "duration": 1226624253,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1092765132,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2059720756,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TEST 1",
      "offset": 26
    },
    {
      "val": "TEST 3",
      "offset": 39
    }
  ],
  "location": "WebsiteStepDefs.CheckFloorsAppears(String,String)"
});
formatter.result({
  "duration": 1176989981,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5544509639,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34011331,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 79397491,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 23166587,
  "status": "passed"
});
formatter.scenario({
  "line": 56,
  "name": "Edit Floors",
  "description": "",
  "id": "building-panel-tests;edit-floors",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 55,
      "name": "@Admin"
    },
    {
      "line": 55,
      "name": "@SmokeTest"
    },
    {
      "line": 55,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 57,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "User click on Areas Tab",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "User Search for autoArea",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "User click on three dots button",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "Enter new name autoArea2",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "Check if floors has the new name autoArea2",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3059703133,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openAreaTab()"
});
formatter.result({
  "duration": 618523253,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea",
      "offset": 16
    }
  ],
  "location": "WebsiteStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2102761644,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.thredotMenu()"
});
formatter.result({
  "duration": 2059000525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea2",
      "offset": 15
    }
  ],
  "location": "WebsiteStepDefs.editfloorfield(String)"
});
formatter.result({
  "duration": 2240015545,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1066060925,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2071580224,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea2",
      "offset": 33
    }
  ],
  "location": "WebsiteStepDefs.CheckFloorNewNameAppears(String)"
});
formatter.result({
  "duration": 1145503865,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6811945914,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33767389,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 76458595,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 19552909,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "Delete Floors",
  "description": "",
  "id": "building-panel-tests;delete-floors",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 66,
      "name": "@Admin"
    },
    {
      "line": 66,
      "name": "@SmokeTest"
    },
    {
      "line": 66,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 68,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "User click on Areas Tab",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "User click Delete button",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "User Select all floors with name autoArea2",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "User click delete",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "Check if floor with name autoArea2 is deleted",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3048947897,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openAreaTab()"
});
formatter.result({
  "duration": 627868171,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2070773267,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabDeleteButton()"
});
formatter.result({
  "duration": 654376204,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea2",
      "offset": 33
    }
  ],
  "location": "WebsiteStepDefs.Selectfield(String)"
});
formatter.result({
  "duration": 4184482148,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.DeleteButton()"
});
formatter.result({
  "duration": 1065732577,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2053854559,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "autoArea2",
      "offset": 25
    }
  ],
  "location": "WebsiteStepDefs.CheckFloorDeleted(String)"
});
formatter.result({
  "duration": 1110204188,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6480073294,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35300605,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71757846,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 19961489,
  "status": "passed"
});
formatter.scenario({
  "line": 78,
  "name": "Assign Users",
  "description": "",
  "id": "building-panel-tests;assign-users",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 77,
      "name": "@Admin"
    },
    {
      "line": 77,
      "name": "@SmokeTest"
    },
    {
      "line": 77,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 79,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "User goto on Users Tab",
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "click the Assign Users button",
  "keyword": "And "
});
formatter.step({
  "line": 83,
  "name": "User select user Edon",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Check if user Edon appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3050885518,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openUsersTab()"
});
formatter.result({
  "duration": 602322919,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2063394260,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AssignUsersButton()"
});
formatter.result({
  "duration": 1111629809,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edon",
      "offset": 17
    }
  ],
  "location": "WebsiteStepDefs.SelectUser(String)"
});
formatter.result({
  "duration": 2169053988,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1068193792,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edon",
      "offset": 14
    }
  ],
  "location": "WebsiteStepDefs.CheckUserAppears(String)"
});
formatter.result({
  "duration": 1099183212,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5271264676,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35196257,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 67389619,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 24374238,
  "status": "passed"
});
formatter.scenario({
  "line": 88,
  "name": "Delete Assign Users",
  "description": "",
  "id": "building-panel-tests;delete-assign-users",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 87,
      "name": "@Admin"
    },
    {
      "line": 87,
      "name": "@SmokeTest"
    },
    {
      "line": 87,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 89,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "User goto on Users Tab",
  "keyword": "And "
});
formatter.step({
  "line": 91,
  "name": "User Search for ttestt",
  "keyword": "And "
});
formatter.step({
  "line": 92,
  "name": "User opens menu popup",
  "keyword": "And "
});
formatter.step({
  "line": 93,
  "name": "click Remove Users button",
  "keyword": "And "
});
formatter.step({
  "line": 94,
  "name": "User select user Edon",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": "User click delete",
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "Check if user Edon disappears from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3049588824,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openUsersTab()"
});
formatter.result({
  "duration": 600692776,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ttestt",
      "offset": 16
    }
  ],
  "location": "WebsiteStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2087991106,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2058964352,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.RemoveUsersButton()"
});
formatter.result({
  "duration": 1114327546,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edon",
      "offset": 17
    }
  ],
  "location": "WebsiteStepDefs.SelectUser(String)"
});
formatter.result({
  "duration": 2144066649,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.DeleteButton()"
});
formatter.result({
  "duration": 1067321908,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edon",
      "offset": 14
    }
  ],
  "location": "WebsiteStepDefs.CheckUserDisappears(String)"
});
formatter.result({
  "duration": 2096849998,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6243574962,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32034290,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 73061034,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 21984444,
  "status": "passed"
});
formatter.scenario({
  "line": 99,
  "name": "Add Connector",
  "description": "",
  "id": "building-panel-tests;add-connector",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 98,
      "name": "@Admin"
    },
    {
      "line": 98,
      "name": "@SmokeTest"
    },
    {
      "line": 98,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 100,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "User goto on Connectors Tab",
  "keyword": "And "
});
formatter.step({
  "line": 102,
  "name": "User click create new connector",
  "keyword": "And "
});
formatter.step({
  "line": 103,
  "name": "User fill mandatory fields for connector and click add",
  "keyword": "And "
});
formatter.step({
  "line": 104,
  "name": "Check if connector with name jace appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3061167247,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openConnectorsTab()"
});
formatter.result({
  "duration": 668885640,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openAddConnector()"
});
formatter.result({
  "duration": 158548259,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.addnewConnector()"
});
formatter.result({
  "duration": 6088425657,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "jace",
      "offset": 29
    }
  ],
  "location": "WebsiteStepDefs.checkConnector(String)"
});
formatter.result({
  "duration": 1023551051,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6395685198,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35014460,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 72563875,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20050068,
  "status": "passed"
});
formatter.scenario({
  "line": 114,
  "name": "Edit Connector",
  "description": "",
  "id": "building-panel-tests;edit-connector",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 113,
      "name": "@Admin"
    },
    {
      "line": 113,
      "name": "@SmokeTest"
    },
    {
      "line": 113,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 115,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 116,
  "name": "User goto on Connectors Tab",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": "User click edit connector",
  "keyword": "And "
});
formatter.step({
  "line": 118,
  "name": "User change connector name to AutoJace",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "Check if connector with name AutoJace appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3051423952,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openConnectorsTab()"
});
formatter.result({
  "duration": 595047333,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickEditConnector()"
});
formatter.result({
  "duration": 1735654274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoJace",
      "offset": 30
    }
  ],
  "location": "WebsiteStepDefs.editExistingConnector(String)"
});
formatter.result({
  "duration": 2244346672,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoJace",
      "offset": 29
    }
  ],
  "location": "WebsiteStepDefs.checkConnector(String)"
});
formatter.result({
  "duration": 1024271746,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7212107862,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34943968,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 74694423,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 21233140,
  "status": "passed"
});
formatter.scenario({
  "line": 122,
  "name": "Delete Connector",
  "description": "",
  "id": "building-panel-tests;delete-connector",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 121,
      "name": "@Admin"
    },
    {
      "line": 121,
      "name": "@SmokeTest"
    },
    {
      "line": 121,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 123,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 124,
  "name": "User goto on Connectors Tab",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "User delete connector from building",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": "Check if connector with name AutoJace disappears from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3052171546,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openConnectorsTab()"
});
formatter.result({
  "duration": 621386554,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickDeleteExistingConnector()"
});
formatter.result({
  "duration": 1764828044,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoJace",
      "offset": 29
    }
  ],
  "location": "WebsiteStepDefs.checkDeletedConnector(String)"
});
formatter.result({
  "duration": 1011805670,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6251292985,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35185591,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 75888162,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 29264669,
  "status": "passed"
});
formatter.scenario({
  "line": 129,
  "name": "Delete Building",
  "description": "",
  "id": "building-panel-tests;delete-building",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 128,
      "name": "@Admin"
    },
    {
      "line": 128,
      "name": "@SmokeTest"
    },
    {
      "line": 128,
      "name": "@BuildingPanel"
    }
  ]
});
formatter.step({
  "line": 130,
  "name": "User Open Auto Test Building",
  "keyword": "When "
});
formatter.step({
  "line": 131,
  "name": "Click on Delete Button, then click Delete again",
  "keyword": "And "
});
formatter.step({
  "line": 132,
  "name": "Check if Auto Test Building is removed from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 3051725865,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.deleteBuilding()"
});
formatter.result({
  "duration": 684983016,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Test Building",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.checkDeletedBuilding(String)"
});
formatter.result({
  "duration": 4013879175,
  "status": "passed"
});
formatter.uri("DeviceDetails.feature");
formatter.feature({
  "line": 1,
  "name": "Devices Tests",
  "description": "Check Devices list functions, Check Filters, Check Device Batch Update, Check Point Batch Update, Check Device Details Page.",
  "id": "devices-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6835763167,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35679039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2178979544,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1842706696,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1655393247,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1022798819,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Search For Device",
  "description": "",
  "id": "devices-tests;search-for-device",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@SmokeTest"
    },
    {
      "line": 12,
      "name": "@Devices"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "User is Searching for Flex",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Check if Flex Device appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2076813379,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckDeviceisAppeared(String)"
});
formatter.result({
  "duration": 1022896674,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5986425984,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34295620,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2180047601,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1823082367,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1650239861,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021568907,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Filter By Type",
  "description": "",
  "id": "devices-tests;filter-by-type",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@SmokeTest"
    },
    {
      "line": 17,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "User clicks on Filters button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Click Filter by FCU type",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Check if all Devices with FCU type appears",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.goToFilters()"
});
formatter.result({
  "duration": 117947717,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FCU",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.selectFilter(String)"
});
formatter.result({
  "duration": 651588961,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FCU",
      "offset": 26
    }
  ],
  "location": "DeviceStepDefs.CheckType(String)"
});
formatter.result({
  "duration": 1026286353,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6825147989,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 39431383,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2167701178,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1783459911,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1638179118,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021014242,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Filter by Area",
  "description": "",
  "id": "devices-tests;filter-by-area",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 23,
      "name": "@SmokeTest"
    },
    {
      "line": 23,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "User clicks on Filters button",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "Click Filter by Basement area",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "Check if all Devices with Basement area appears",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.goToFilters()"
});
formatter.result({
  "duration": 98757938,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.filterArea(String)"
});
formatter.result({
  "duration": 635588512,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 26
    }
  ],
  "location": "DeviceStepDefs.CheckArea(String)"
});
formatter.result({
  "duration": 1024182239,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5517196505,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33882404,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2197793208,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1774587571,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1654359045,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1019423518,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Go to Device Details Page",
  "description": "",
  "id": "devices-tests;go-to-device-details-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@SmokeTest"
    },
    {
      "line": 29,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "User Click on Flex Device",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "Click at Device details button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "Check if Flex Device details page appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 14
    }
  ],
  "location": "DeviceStepDefs.clickDevice(String)"
});
formatter.result({
  "duration": 110129521,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openDeviceDetails()"
});
formatter.result({
  "duration": 124408001,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckDeviceDetails(String)"
});
formatter.result({
  "duration": 1023236152,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6983511668,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34346171,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2179049573,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1874553160,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1642419810,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021969139,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Update Device using Batch Update",
  "description": "",
  "id": "devices-tests;update-device-using-batch-update",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 35,
      "name": "@SmokeTest"
    },
    {
      "line": 35,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 37,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "Click Device batch update button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "User is Searching for Flex",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "Select AutomatedTag as Tag",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "User click Save button",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "User click Confirm button",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "Check if Device has tag AutomationTag",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 108423319,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchDeviceUpdate()"
});
formatter.result({
  "duration": 90200960,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2085131979,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1077508102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomatedTag",
      "offset": 7
    }
  ],
  "location": "DeviceStepDefs.openAreasTab(String)"
});
formatter.result({
  "duration": 3394169605,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 166359962,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.confirmButton()"
});
formatter.result({
  "duration": 1097975561,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomationTag",
      "offset": 24
    }
  ],
  "location": "DeviceStepDefs.CheckTypeIsChanged(String)"
});
formatter.result({
  "duration": 1035918343,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5361840360,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34110577,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2182417454,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1764406944,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1640770188,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1019716156,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Create Point Group and add some points",
  "description": "",
  "id": "devices-tests;create-point-group-and-add-some-points",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 46,
      "name": "@SmokeTest"
    },
    {
      "line": 46,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 48,
  "name": "User Search for BR127",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "User click open BR127 Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "User Click edit groups button",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "User Click Add new Points group button",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "User Drag Point to New Point Group",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "User click Save button",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "Check if Point appears on new Group 1",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "WebsiteStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2076515640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1727852311,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openEditGroupPage()"
});
formatter.result({
  "duration": 2081790533,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openAddPointPage()"
});
formatter.result({
  "duration": 1629128692,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.dragPointToGroup()"
});
formatter.result({
  "duration": 206737431,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 80458128,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group 1",
      "offset": 30
    }
  ],
  "location": "DeviceStepDefs.CheckpointIsChanged(String)"
});
formatter.result({
  "duration": 1012082076,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5403713944,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 30196378,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2189442143,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1780466291,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1643305607,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1022811341,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Delete Points Group",
  "description": "",
  "id": "devices-tests;delete-points-group",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@SmokeTest"
    },
    {
      "line": 56,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "User Search for BR127",
  "keyword": "When "
});
formatter.step({
  "line": 59,
  "name": "User click open BR127 Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "User Click edit groups button",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "User Click delete group",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "User click Save button",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "Checks if Group 1 is deleted",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "WebsiteStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2080866707,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1724294285,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openEditGroupPage()"
});
formatter.result({
  "duration": 2098371155,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.deleteGroup()"
});
formatter.result({
  "duration": 3710166993,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 101881876,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Group 1",
      "offset": 10
    }
  ],
  "location": "DeviceStepDefs.CheckGroupisDeleted(String)"
});
formatter.result({
  "duration": 1008777267,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6254689619,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 41574918,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2195381152,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1788808544,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1684640292,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1020770763,
  "status": "passed"
});
formatter.scenario({
  "line": 66,
  "name": "Add point to Template from Point Batch Update",
  "description": "",
  "id": "devices-tests;add-point-to-template-from-point-batch-update",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 65,
      "name": "@SmokeTest"
    },
    {
      "line": 65,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 67,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "Click Device batch update button",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "User is Searching for BR127",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "Click Apply Point template button",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "Drag Point to Template",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "User click Update Points button",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "User is Searching for BR127",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "Click Apply Point template button",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "Check if Point appears on Template",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 112339838,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchDeviceUpdate()"
});
formatter.result({
  "duration": 107097408,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2128577736,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1090735684,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchPointsUpdate()"
});
formatter.result({
  "duration": 1130259357,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.dragPointToTemplate()"
});
formatter.result({
  "duration": 1248485799,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.TemplateSaveButton()"
});
formatter.result({
  "duration": 94479680,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2103661354,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1072434948,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchPointsUpdate()"
});
formatter.result({
  "duration": 1103054281,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.CheckpointIsInTemplate()"
});
formatter.result({
  "duration": 2012735060,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5460710066,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34463968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2221123041,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1802434502,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1655194291,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021362067,
  "status": "passed"
});
formatter.scenario({
  "line": 80,
  "name": "Remove point from Template at Point Batch Update",
  "description": "",
  "id": "devices-tests;remove-point-from-template-at-point-batch-update",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 79,
      "name": "@SmokeTest"
    },
    {
      "line": 79,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "Click Device batch update button",
  "keyword": "And "
});
formatter.step({
  "line": 83,
  "name": "User is Searching for BR127",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Click Apply Point template button",
  "keyword": "And "
});
formatter.step({
  "line": 86,
  "name": "Remove Point from Template",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": "User click Update Points button",
  "keyword": "And "
});
formatter.step({
  "line": 88,
  "name": "User is Searching for BR127",
  "keyword": "And "
});
formatter.step({
  "line": 89,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 90,
  "name": "Click Apply Point template button",
  "keyword": "And "
});
formatter.step({
  "line": 91,
  "name": "Check if Point disappears from Template",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 104877815,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchDeviceUpdate()"
});
formatter.result({
  "duration": 90747278,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2100160371,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1075732336,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchPointsUpdate()"
});
formatter.result({
  "duration": 1106521872,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.removePointToTemplate()"
});
formatter.result({
  "duration": 1236793752,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.TemplateSaveButton()"
});
formatter.result({
  "duration": 81358765,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2091306119,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1069521559,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.batchPointsUpdate()"
});
formatter.result({
  "duration": 1101417181,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.CheckpointIsDeletedFromTemplate()"
});
formatter.result({
  "duration": 1009746541,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5279546639,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33790113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2216393539,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1794372828,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1660452489,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021637082,
  "status": "passed"
});
formatter.scenario({
  "line": 94,
  "name": "Update Device from device page",
  "description": "",
  "id": "devices-tests;update-device-from-device-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 93,
      "name": "@SmokeTest"
    },
    {
      "line": 93,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 95,
  "name": "User click on three dots button",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "User click edit button",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "Select AutomationTag as Tags",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": "User click Save button",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "User click open BR126 Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 100,
  "name": "Open Device Details Pop-up",
  "keyword": "And "
});
formatter.step({
  "line": 101,
  "name": "Check if Device has tag AutomationTag",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thredotMenu()"
});
formatter.result({
  "duration": 2069254892,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openEditButton()"
});
formatter.result({
  "duration": 2151298178,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomationTag",
      "offset": 7
    }
  ],
  "location": "DeviceStepDefs.openTagTab(String)"
});
formatter.result({
  "duration": 3437175711,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 114214387,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1245460178,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.ConfirmClick()"
});
formatter.result({
  "duration": 1090062293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomationTag",
      "offset": 24
    }
  ],
  "location": "DeviceStepDefs.CheckTypeIsChanged(String)"
});
formatter.result({
  "duration": 1024183630,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5413050515,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 31905826,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2187757739,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1793478684,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1645288677,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1021052271,
  "status": "passed"
});
formatter.scenario({
  "line": 104,
  "name": "Update Device from Details page",
  "description": "",
  "id": "devices-tests;update-device-from-details-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 103,
      "name": "@SmokeTest"
    },
    {
      "line": 103,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 105,
  "name": "User Search for Flex",
  "keyword": "When "
});
formatter.step({
  "line": 106,
  "name": "User click open Flex Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 107,
  "name": "User Click edit Device button",
  "keyword": "And "
});
formatter.step({
  "line": 108,
  "name": "Select AutomationTag as Tags",
  "keyword": "And "
});
formatter.step({
  "line": 109,
  "name": "User click Save button",
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "Open Device Details Pop-up",
  "keyword": "And "
});
formatter.step({
  "line": 111,
  "name": "Check if Device has tag AutomationTag",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 16
    }
  ],
  "location": "WebsiteStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2084564791,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Flex",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1728800251,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openEditPage()"
});
formatter.result({
  "duration": 2121009974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomationTag",
      "offset": 7
    }
  ],
  "location": "DeviceStepDefs.openTagTab(String)"
});
formatter.result({
  "duration": 3445570833,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 107739726,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.ConfirmClick()"
});
formatter.result({
  "duration": 1092799914,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutomationTag",
      "offset": 24
    }
  ],
  "location": "DeviceStepDefs.CheckTypeIsChanged(String)"
});
formatter.result({
  "duration": 1020854241,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5081193663,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34242287,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2177575719,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1773898876,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1648830471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1020222127,
  "status": "passed"
});
formatter.scenario({
  "line": 114,
  "name": "Discover Devices",
  "description": "",
  "id": "devices-tests;discover-devices",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 113,
      "name": "@SmokeTest"
    },
    {
      "line": 113,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 115,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 116,
  "name": "Click Discover Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": "Check if Device are Discover successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 108258682,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.discoverDevices()"
});
formatter.result({
  "duration": 709949022,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.CheckDiscover()"
});
formatter.result({
  "duration": 1029076379,
  "status": "passed"
});
formatter.uri("FloorDetails.feature");
formatter.feature({
  "line": 1,
  "name": "Floor Tests",
  "description": "Go To Floor, add plan, add zones, add devices at different tabs, check Hide Zones and Names etc.",
  "id": "floor-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Clicks Basement from the list",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6190655823,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 36684952,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2178354385,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1762008337,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2106279321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1022202414,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Add Floor plan",
  "description": "",
  "id": "floor-tests;add-floor-plan",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@SmokeTest"
    },
    {
      "line": 12,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "User go to edit page",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "User upload floor plan",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User save the changes at Floor",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Check if floor plan appears",
  "keyword": "Then "
});
formatter.match({
  "location": "FloorStepDefs.goToEditPage()"
});
formatter.result({
  "duration": 122380408,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.uploadFloorPlan()"
});
formatter.result({
  "duration": 4058066034,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.saveChanges()"
});
formatter.result({
  "duration": 4191142315,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.CheckFloorPlan()"
});
formatter.result({
  "duration": 999825159,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Clicks Basement from the list",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5649157890,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33346752,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2174407259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1763744219,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2103946570,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1020008329,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Add Devices at Floor",
  "description": "",
  "id": "floor-tests;add-devices-at-floor",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 19,
      "name": "@SmokeTest"
    },
    {
      "line": 19,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "User go to edit page",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "user go to list view",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user drag and drop the device to floor",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "user drag and drop point to device",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "User save the changes at Floor",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "Check if device appears at floor plan",
  "keyword": "Then "
});
formatter.match({
  "location": "FloorStepDefs.goToEditPage()"
});
formatter.result({
  "duration": 109883724,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.goToListView()"
});
formatter.result({
  "duration": 1111023666,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.dragDeviceToFloor()"
});
formatter.result({
  "duration": 2187725275,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.dragPointToDevice()"
});
formatter.result({
  "duration": 1134097035,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.saveChanges()"
});
formatter.result({
  "duration": 4139744334,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.checkDeviceAppearsAtFloor()"
});
formatter.result({
  "duration": 2000891825,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Clicks Basement from the list",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6998941218,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33760897,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2173107781,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1751900056,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2107335784,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1021747922,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Hide Zones and Names at Floor",
  "description": "",
  "id": "floor-tests;hide-zones-and-names-at-floor",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 28,
      "name": "@SmokeTest"
    },
    {
      "line": 28,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 30,
  "name": "User click hide Names",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "Check if Names are hided from Floor",
  "keyword": "Then "
});
formatter.match({
  "location": "FloorStepDefs.hideNames()"
});
formatter.result({
  "duration": 67461039,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.checkNamesAreHidden()"
});
formatter.result({
  "duration": 1999734261,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Clicks Basement from the list",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Check if Basement page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5248301682,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 37981182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2180267891,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1918237757,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 7
    }
  ],
  "location": "DashboardStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 2112760012,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 9
    }
  ],
  "location": "DashboardStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1024527282,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Reset Floor to Default",
  "description": "",
  "id": "floor-tests;reset-floor-to-default",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 33,
      "name": "@SmokeTest"
    },
    {
      "line": 33,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 35,
  "name": "User go to edit page",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "user Click reset floor to default",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "User save the changes at Floor",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "Check if Floor is reseted to default",
  "keyword": "Then "
});
formatter.match({
  "location": "FloorStepDefs.goToEditPage()"
});
formatter.result({
  "duration": 126489853,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.resetFloor()"
});
formatter.result({
  "duration": 3237808475,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.saveChanges()"
});
formatter.result({
  "duration": 4120632005,
  "status": "passed"
});
formatter.match({
  "location": "FloorStepDefs.checkFloorIsResseted()"
});
formatter.result({
  "duration": 1999604406,
  "status": "passed"
});
formatter.uri("PointsDetails.feature");
formatter.feature({
  "line": 1,
  "name": "Points Tests",
  "description": "Check Devices list functions, Check Filters, Check Device Batch Update, Check Point Batch Update, Check Device Details Page.",
  "id": "points-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5624480348,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 36831502,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2178907660,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1876913273,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Connect Points from Edit Points page",
  "description": "",
  "id": "points-tests;connect-points-from-edit-points-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@SmokeTest"
    },
    {
      "line": 11,
      "name": "@Points"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "User is Searching for BR127",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "Check if BR127 Device appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "User click open BR127 Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User Click edit Points button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User click connect points button",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User connect point BO1 with DEV277127~2dsystemStatus",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "User Click edit Points button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User click connect points button",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User connect point BO1 with DEV277127~2dsystemStatus",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "Check if points are connected",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1698306599,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1025657019,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2085690819,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckDeviceisAppeared(String)"
});
formatter.result({
  "duration": 1023721254,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1714335338,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.editPointButton()"
});
formatter.result({
  "duration": 2156403333,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.connectPointsButton()"
});
formatter.result({
  "duration": 91696610,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 19
    },
    {
      "val": "DEV277127~2dsystemStatus",
      "offset": 28
    }
  ],
  "location": "PointsStepDefs.connectTwoPoints(String,String)"
});
formatter.result({
  "duration": 3217767682,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.editPointButton()"
});
formatter.result({
  "duration": 2077321204,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.connectPointsButton()"
});
formatter.result({
  "duration": 69396341,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 19
    },
    {
      "val": "DEV277127~2dsystemStatus",
      "offset": 28
    }
  ],
  "location": "PointsStepDefs.connectTwoPoints(String,String)"
});
formatter.result({
  "duration": 3251876868,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.checkPointsConnected()"
});
formatter.result({
  "duration": 1024386296,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6717327103,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 38437066,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2172094449,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1851605007,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Edit Point Using Point batch Update",
  "description": "",
  "id": "points-tests;edit-point-using-point-batch-update",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@SmokeTest"
    },
    {
      "line": 26,
      "name": "@Points"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "Click Point batch update button",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "User is Searching for BO1",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "add Tag Auto and update point",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "User is Searching for BO1",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Click checkbox",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "remove Tag Auto and update point",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "User is Searching for BO1",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "Check if Point is updated",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1664296195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1024415978,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 106664249,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.pointBatchUpdate()"
});
formatter.result({
  "duration": 89964438,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2097057765,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1084839342,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto",
      "offset": 8
    }
  ],
  "location": "PointsStepDefs.addTag(String)"
});
formatter.result({
  "duration": 3543950336,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2116921862,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 1069507182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto",
      "offset": 11
    }
  ],
  "location": "PointsStepDefs.removeTag(String)"
});
formatter.result({
  "duration": 3519184678,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2128782258,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.CheckPointUpdated()"
});
formatter.result({
  "duration": 1016973433,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5733157813,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 39040891,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2171250856,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1828879926,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "Edit Point at Device Details page",
  "description": "",
  "id": "points-tests;edit-point-at-device-details-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 41,
      "name": "@SmokeTest"
    },
    {
      "line": 41,
      "name": "@Points"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 45,
  "name": "User is Searching for BR127",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "Check if BR127 Device appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "User click open BR127 Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "User Click edit Points button",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "Select Point with name BO1",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "Change point name to BO12",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "Check if point name is changed to BO12",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "Select Point with name BO12",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "Change point name to BO1",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "Check if point name is changed to BO1",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1653358698,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1020755922,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2084067169,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckDeviceisAppeared(String)"
});
formatter.result({
  "duration": 1022355458,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR127",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openDevicepage(String)"
});
formatter.result({
  "duration": 1748067480,
  "status": "passed"
});
formatter.match({
  "location": "PointsStepDefs.editPointButton()"
});
formatter.result({
  "duration": 2174999490,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 23
    }
  ],
  "location": "PointsStepDefs.selectPoint(String)"
});
formatter.result({
  "duration": 2104272135,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO12",
      "offset": 21
    }
  ],
  "location": "PointsStepDefs.changePointName(String)"
});
formatter.result({
  "duration": 2202287581,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO12",
      "offset": 34
    }
  ],
  "location": "PointsStepDefs.checkPoint(String)"
});
formatter.result({
  "duration": 2012191525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO12",
      "offset": 23
    }
  ],
  "location": "PointsStepDefs.selectPoint(String)"
});
formatter.result({
  "duration": 2083719342,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 21
    }
  ],
  "location": "PointsStepDefs.changePointName(String)"
});
formatter.result({
  "duration": 2186656290,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 34
    }
  ],
  "location": "PointsStepDefs.checkPoint(String)"
});
formatter.result({
  "duration": 2013828625,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6888508857,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 37590226,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2164151499,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1892398477,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Open Point detail pop-up",
  "description": "",
  "id": "points-tests;open-point-detail-pop-up",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@SmokeTest"
    },
    {
      "line": 56,
      "name": "@Points"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "User Click on Points button",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "Check if the list of BO1 appears",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "User is Searching for BO1",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "Check if BO1 Device appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "User click open BO1 point pop-up",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "Check if BO1 popup appears",
  "keyword": "Then "
});
formatter.match({
  "location": "DashboardStepDefs.clickPoint()"
});
formatter.result({
  "duration": 1133234427,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1024017138,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 22
    }
  ],
  "location": "DeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 2086978702,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckDeviceisAppeared(String)"
});
formatter.result({
  "duration": 1022129139,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 16
    }
  ],
  "location": "DeviceStepDefs.openPoint(String)"
});
formatter.result({
  "duration": 2091282466,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BO1",
      "offset": 9
    }
  ],
  "location": "DeviceStepDefs.CheckPointPopup(String)"
});
formatter.result({
  "duration": 1016550942,
  "status": "passed"
});
formatter.uri("Search.feature");
formatter.feature({
  "line": 1,
  "name": "Search Tests",
  "description": "Check The Search functions",
  "id": "search-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5515595116,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35709184,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Search has 3 different part",
  "description": "",
  "id": "search-tests;search-has-3-different-part",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@SmokeTest"
    },
    {
      "line": 8,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Check if by default there appears the list of Building, Floors and Devices",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 103127093,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.openBuilding()"
});
formatter.result({
  "duration": 1086570123,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 4802958560,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 37254922,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "View all for Buildings",
  "description": "",
  "id": "search-tests;view-all-for-buildings",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 13,
      "name": "@SmokeTest"
    },
    {
      "line": 13,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 15,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User click on View all button for Buildings",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Check if All buildings appears on the list",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 89456149,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.openAreaTab()"
});
formatter.result({
  "duration": 1076229959,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.openMenuTab()"
});
formatter.result({
  "duration": 2018408795,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6698360396,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32913129,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "View all for Floors",
  "description": "",
  "id": "search-tests;view-all-for-floors",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 19,
      "name": "@SmokeTest"
    },
    {
      "line": 19,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "User click on View all button for Floors",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "Check if All Floors appears on the list",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 77311465,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.AreaTabSaveButton()"
});
formatter.result({
  "duration": 1066435185,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.SaveButton()"
});
formatter.result({
  "duration": 2022258530,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8436337219,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32800897,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "View all for Devices",
  "description": "",
  "id": "search-tests;view-all-for-devices",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 25,
      "name": "@SmokeTest"
    },
    {
      "line": 25,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 27,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "User click on View all button for Devices",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Check if All Devices appears on the list",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 74663351,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.DeleteButton()"
});
formatter.result({
  "duration": 1078981956,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDefs.AssignUsersButton()"
});
formatter.result({
  "duration": 2020204039,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8617586905,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34639273,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "User Search with Building name",
  "description": "",
  "id": "search-tests;user-search-with-building-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 31,
      "name": "@SmokeTest"
    },
    {
      "line": 31,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 33,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "search for Automation Testing",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Search results shows the Building with Automation search keyword, also shows the Floors and Devices for that Building",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 103557932,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 11
    }
  ],
  "location": "SearchStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1101815558,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation",
      "offset": 39
    }
  ],
  "location": "SearchStepDefs.seeresults(String)"
});
formatter.result({
  "duration": 1063294086,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7828868796,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33706635,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "User Search with Floor name",
  "description": "",
  "id": "search-tests;user-search-with-floor-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 37,
      "name": "@SmokeTest"
    },
    {
      "line": 37,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "search for Basement",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "Search results shows all floors with Basement search keyword, and all devices that are assign to that floor",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 77737667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 11
    }
  ],
  "location": "SearchStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1062880869,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Basement",
      "offset": 37
    }
  ],
  "location": "SearchStepDefs.EnterSVG(String)"
});
formatter.result({
  "duration": 1021716850,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6900143861,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33691795,
  "status": "passed"
});
formatter.scenario({
  "line": 44,
  "name": "User Search with Devices name",
  "description": "",
  "id": "search-tests;user-search-with-devices-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 43,
      "name": "@SmokeTest"
    },
    {
      "line": 43,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "User clicks on Search button",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "search for BR126",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "Search results shows all devices with BR126 search keyword",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchStepDefs.theNextStepThatGetsRepeatedBeforeEveryTest()"
});
formatter.result({
  "duration": 74524685,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 11
    }
  ],
  "location": "SearchStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1064060694,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BR126",
      "offset": 38
    }
  ],
  "location": "SearchStepDefs.addfloorfield(String)"
});
formatter.result({
  "duration": 1529459451,
  "status": "passed"
});
formatter.uri("Sites.feature");
formatter.feature({
  "line": 1,
  "name": "Sites Page Tests",
  "description": "Check The Search functions",
  "id": "sites-page-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5860440246,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33600896,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Search for Building",
  "description": "",
  "id": "sites-page-tests;search-for-building",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@SmokeTest"
    },
    {
      "line": 8,
      "name": "@Sites"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "User At Home page search for Chrysler House",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Check if Chrysler House appears on Search list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 29
    }
  ],
  "location": "SitesStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1198190193,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openBuilding(String)"
});
formatter.result({
  "duration": 1030736667,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 9881355704,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32426636,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Go To Building Dashboard",
  "description": "",
  "id": "sites-page-tests;go-to-building-dashboard",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 13,
      "name": "@SmokeTest"
    },
    {
      "line": 13,
      "name": "@Sites"
    }
  ]
});
formatter.step({
  "line": 15,
  "name": "User At Home page search for Chrysler House",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User click twice on Chrysler House building",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Check if Chrysler House Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 29
    }
  ],
  "location": "SitesStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1181366093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2139706769,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1871067482,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8120947366,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32829651,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Select Building from Map",
  "description": "",
  "id": "sites-page-tests;select-building-from-map",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 19,
      "name": "@SmokeTest"
    },
    {
      "line": 19,
      "name": "@Sites"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "User At Home page search for Chrysler House",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "Select Chrysler House from Map",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "Check if Chrysler House Dashboard appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 29
    }
  ],
  "location": "SitesStepDefs.Searchfields(String)"
});
formatter.result({
  "duration": 1174891895,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 7
    }
  ],
  "location": "SitesStepDefs.AreaTabSaveButton(String)"
});
formatter.result({
  "duration": 1283167274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1812747768,
  "status": "passed"
});
formatter.uri("Systems.feature");
formatter.feature({
  "line": 1,
  "name": "Systems Tests",
  "description": "Check Devices list functions, Check Filters, Check Device Batch Update, Check Point Batch Update, Check Device Details Page.",
  "id": "systems-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Systems button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if Systems page appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8034863736,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33003563,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2192032285,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1793326568,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.clickSystems()"
});
formatter.result({
  "duration": 2155555566,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkSys()"
});
formatter.result({
  "duration": 1024098761,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Create System and Add content",
  "description": "",
  "id": "systems-tests;create-system-and-add-content",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@SmokeTest"
    },
    {
      "line": 12,
      "name": "@Devices"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "User click Add new System button",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "User fill mandatory fields for System and click add",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Check if Automation System is created",
  "keyword": "Then "
});
formatter.match({
  "location": "SystemsStepDefs.addSystem()"
});
formatter.result({
  "duration": 2101564659,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.fillMandatory()"
});
formatter.result({
  "duration": 4889811116,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation System",
      "offset": 9
    }
  ],
  "location": "SystemsStepDefs.checkSystem(String)"
});
formatter.result({
  "duration": 1025012846,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Systems button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if Systems page appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6707453953,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 31400319,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2187433101,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1762028279,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.clickSystems()"
});
formatter.result({
  "duration": 2648481718,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkSys()"
});
formatter.result({
  "duration": 1021061082,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Add content to System",
  "description": "",
  "id": "systems-tests;add-content-to-system",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 18,
      "name": "@SmokeTest"
    },
    {
      "line": 18,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 20,
  "name": "User clicks Automation System from the list and go to Details page",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "User Click Edit Content button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "User add Device with Linked points",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "User Drag Device to the list",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User Drag point to device",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "User Save changes for System",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "Check if Changes Appears on System",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Automation System",
      "offset": 12
    }
  ],
  "location": "SystemsStepDefs.clickSystem(String)"
});
formatter.result({
  "duration": 2185539074,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.editContent()"
});
formatter.result({
  "duration": 2119094151,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.addDeviceWithPoint()"
});
formatter.result({
  "duration": 2119549570,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.dragDevice()"
});
formatter.result({
  "duration": 160029069,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.dragPoint()"
});
formatter.result({
  "duration": 2124706204,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.saveChanges()"
});
formatter.result({
  "duration": 1074019178,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkChanges()"
});
formatter.result({
  "duration": 1024380267,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Systems button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if Systems page appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6190431359,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 43035323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2189064173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1765178189,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.clickSystems()"
});
formatter.result({
  "duration": 2645124039,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkSys()"
});
formatter.result({
  "duration": 1024212383,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Edit System",
  "description": "",
  "id": "systems-tests;edit-system",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 28,
      "name": "@SmokeTest"
    },
    {
      "line": 28,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 30,
  "name": "User clicks Automation System from the list and go to Details page",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "User Click Edit System button",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "User add Automation as Tag",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "Check if Changes Appears on System",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Automation System",
      "offset": 12
    }
  ],
  "location": "SystemsStepDefs.clickSystem(String)"
});
formatter.result({
  "duration": 2174945229,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.editButton()"
});
formatter.result({
  "duration": 2103781005,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation",
      "offset": 9
    }
  ],
  "location": "SystemsStepDefs.addTag(String)"
});
formatter.result({
  "duration": 3463761193,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkChanges()"
});
formatter.result({
  "duration": 1019009373,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Systems button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if Systems page appears",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6592079683,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34279388,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2185269625,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1772014589,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.clickSystems()"
});
formatter.result({
  "duration": 2648410761,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkSys()"
});
formatter.result({
  "duration": 1021261429,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Delete Systems",
  "description": "",
  "id": "systems-tests;delete-systems",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 35,
      "name": "@SmokeTest"
    },
    {
      "line": 35,
      "name": "@Device"
    }
  ]
});
formatter.step({
  "line": 37,
  "name": "User clicks Automation System from the list and go to Details page",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "user click delete System Button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "Check if Systems page appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Automation System",
      "offset": 12
    }
  ],
  "location": "SystemsStepDefs.clickSystem(String)"
});
formatter.result({
  "duration": 2176986735,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.deleteButton()"
});
formatter.result({
  "duration": 2171628363,
  "status": "passed"
});
formatter.match({
  "location": "SystemsStepDefs.checkSys()"
});
formatter.result({
  "duration": 1023680906,
  "status": "passed"
});
formatter.uri("Tags.feature");
formatter.feature({
  "line": 1,
  "name": "Tags Tests",
  "description": "",
  "id": "tags-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Tags tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Tags page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 5559846438,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 37608777,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 72644107,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 23044616,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTags()"
});
formatter.result({
  "duration": 92934870,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTagsPage()"
});
formatter.result({
  "duration": 23443920,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Add New Tag",
  "description": "",
  "id": "tags-tests;add-new-tag",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Admin"
    },
    {
      "line": 11,
      "name": "@SmokeTest"
    },
    {
      "line": 11,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "User Click Add New Tag Button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user add AutoTag as Tag Name and click Add",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Check if Tag with name AutoTag appears on list",
  "keyword": "Then "
});
formatter.match({
  "location": "TagsStepDefs.newTag()"
});
formatter.result({
  "duration": 123592233,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 9
    }
  ],
  "location": "TagsStepDefs.addTag(String)"
});
formatter.result({
  "duration": 3380307589,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.CheckTag(String)"
});
formatter.result({
  "duration": 530212610,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Tags tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Tags page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6794952074,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33579563,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 67477735,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 18823866,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTags()"
});
formatter.result({
  "duration": 87506006,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTagsPage()"
});
formatter.result({
  "duration": 25084267,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Search Tag",
  "description": "",
  "id": "tags-tests;search-tag",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@Admin"
    },
    {
      "line": 17,
      "name": "@SmokeTest"
    },
    {
      "line": 17,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "At Tags page User Search for Tag with name AutoTag",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Check if Tag with name AutoTag appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 43
    }
  ],
  "location": "TagsStepDefs.searchTag(String)"
});
formatter.result({
  "duration": 5121566960,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.CheckTag(String)"
});
formatter.result({
  "duration": 19329373,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Tags tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Tags page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6931821976,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32656665,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71049673,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 18887867,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTags()"
});
formatter.result({
  "duration": 87120151,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTagsPage()"
});
formatter.result({
  "duration": 18003925,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Edit Tag",
  "description": "",
  "id": "tags-tests;edit-tag",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@Admin"
    },
    {
      "line": 22,
      "name": "@SmokeTest"
    },
    {
      "line": 22,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "At Tags page User Search for Tag with name AutoTag",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Check if Tag with name AutoTag appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "User hover at AutoTag and click Edit Button",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "User Change tag name to AutoTag2",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Check if Tag with name AutoTag2 appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 43
    }
  ],
  "location": "TagsStepDefs.searchTag(String)"
});
formatter.result({
  "duration": 5117893919,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.CheckTag(String)"
});
formatter.result({
  "duration": 31805652,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag",
      "offset": 14
    }
  ],
  "location": "TagsStepDefs.editTag(String)"
});
formatter.result({
  "duration": 2177923545,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 24
    }
  ],
  "location": "TagsStepDefs.changeTag(String)"
});
formatter.result({
  "duration": 1162304313,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.CheckTag(String)"
});
formatter.result({
  "duration": 1579220332,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Tags tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Tags page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6800616069,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34690287,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 66271011,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 16512448,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTags()"
});
formatter.result({
  "duration": 84129777,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTagsPage()"
});
formatter.result({
  "duration": 20907111,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Delete Tag",
  "description": "",
  "id": "tags-tests;delete-tag",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 30,
      "name": "@Admin"
    },
    {
      "line": 30,
      "name": "@SmokeTest"
    },
    {
      "line": 30,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 32,
  "name": "At Tags page User Search for Tag with name AutoTag2",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "Check if Tag with name AutoTag2 appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "User hover at AutoTag2 and click Delete Button",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Check if Tag with name AutoTag2 disappears from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 43
    }
  ],
  "location": "TagsStepDefs.searchTag(String)"
});
formatter.result({
  "duration": 5125505274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.CheckTag(String)"
});
formatter.result({
  "duration": 20335286,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 14
    }
  ],
  "location": "TagsStepDefs.deleteTag(String)"
});
formatter.result({
  "duration": 2207972445,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoTag2",
      "offset": 23
    }
  ],
  "location": "TagsStepDefs.checkTagDeleted(String)"
});
formatter.result({
  "duration": 3015414710,
  "status": "passed"
});
formatter.uri("Templates.feature");
formatter.feature({
  "line": 1,
  "name": "Templates Tests",
  "description": "",
  "id": "templates-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6848788546,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 37256314,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 64984520,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20964154,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 92141828,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 535299214,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Add New Section and Add new Point",
  "description": "",
  "id": "templates-tests;add-new-section-and-add-new-point",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Admin"
    },
    {
      "line": 11,
      "name": "@SmokeTest"
    },
    {
      "line": 11,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Click add new section button",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "enter Auto for this section name",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Check if template with Auto name appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "Click add new point button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "Fill point mandatory fields with name NewPoint",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Check if Point with name NewPoint is added on Section with name Auto",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1675954388,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openNewSection()"
});
formatter.result({
  "duration": 106997234,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto",
      "offset": 6
    }
  ],
  "location": "WebsiteStepDefs.enterTemplateName(String)"
});
formatter.result({
  "duration": 1146017256,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto",
      "offset": 23
    }
  ],
  "location": "WebsiteStepDefs.checkTemplatedIsAdded(String)"
});
formatter.result({
  "duration": 534545591,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.addNewPointButton()"
});
formatter.result({
  "duration": 2204367115,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint",
      "offset": 38
    }
  ],
  "location": "WebsiteStepDefs.fillPointFields(String)"
});
formatter.result({
  "duration": 1276035918,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint",
      "offset": 25
    },
    {
      "val": "Auto",
      "offset": 64
    }
  ],
  "location": "WebsiteStepDefs.checkpointisadded(String,String)"
});
formatter.result({
  "duration": 24302818,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6275121831,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33925997,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 74572453,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 21717314,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 80172447,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 531993942,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Edit Section Name",
  "description": "",
  "id": "templates-tests;edit-section-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 21,
      "name": "@Admin"
    },
    {
      "line": 21,
      "name": "@SmokeTest"
    },
    {
      "line": 21,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "Click add new section button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "enter Auto2 for this section name",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "Check if template with Auto2 name appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "Click add new point button",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Fill point mandatory fields with name NewPoint2",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Check if Point with name NewPoint2 is added on Section with name Auto2",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "Click on Edit Button, then change section name to AutoSection2",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "Check if template with AutoSection2 name appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1660167272,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.openNewSection()"
});
formatter.result({
  "duration": 110773695,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto2",
      "offset": 6
    }
  ],
  "location": "WebsiteStepDefs.enterTemplateName(String)"
});
formatter.result({
  "duration": 1160775272,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto2",
      "offset": 23
    }
  ],
  "location": "WebsiteStepDefs.checkTemplatedIsAdded(String)"
});
formatter.result({
  "duration": 533891679,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.addNewPointButton()"
});
formatter.result({
  "duration": 2186591827,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint2",
      "offset": 38
    }
  ],
  "location": "WebsiteStepDefs.fillPointFields(String)"
});
formatter.result({
  "duration": 1296169928,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint2",
      "offset": 25
    },
    {
      "val": "Auto2",
      "offset": 65
    }
  ],
  "location": "WebsiteStepDefs.checkpointisadded(String,String)"
});
formatter.result({
  "duration": 20945140,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoSection2",
      "offset": 50
    }
  ],
  "location": "WebsiteStepDefs.changeSectionName(String)"
});
formatter.result({
  "duration": 5906837883,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoSection2",
      "offset": 23
    }
  ],
  "location": "WebsiteStepDefs.checkTemplatedIsAdded(String)"
});
formatter.result({
  "duration": 20708155,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 9072050020,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 30145363,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 72505904,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 27359974,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 107218915,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 533175157,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Edit Point",
  "description": "",
  "id": "templates-tests;edit-point",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 33,
      "name": "@Admin"
    },
    {
      "line": 33,
      "name": "@SmokeTest"
    },
    {
      "line": 33,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 35,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "Click on Edit Button, then change point name to EditedPoint",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "Check if Point name is changed from NewPoint to EditedPoint",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1662212024,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "EditedPoint",
      "offset": 48
    }
  ],
  "location": "WebsiteStepDefs.changePointName(String)"
});
formatter.result({
  "duration": 397819458,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint",
      "offset": 36
    },
    {
      "val": "EditedPoint",
      "offset": 48
    }
  ],
  "location": "WebsiteStepDefs.checkpointIsEdited(String,String)"
});
formatter.result({
  "duration": 1046600305,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8040999382,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 36374691,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 69217327,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 19233837,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 85206182,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 532135854,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "Link Points",
  "description": "",
  "id": "templates-tests;link-points",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 39,
      "name": "@Admin"
    },
    {
      "line": 39,
      "name": "@SmokeTest"
    },
    {
      "line": 39,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "user select Connect Points",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user select point with name EditedPoint",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "it links it with point with name NewPoint2",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "Click Connect Button",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "Check if points EditedPoint and NewPoint2 are linked",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1657066984,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.connectPointsButton()"
});
formatter.result({
  "duration": 89529424,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "EditedPoint",
      "offset": 28
    }
  ],
  "location": "WebsiteStepDefs.selectFirstPoint(String)"
});
formatter.result({
  "duration": 96882461,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NewPoint2",
      "offset": 33
    }
  ],
  "location": "WebsiteStepDefs.selectSecondPoint(String)"
});
formatter.result({
  "duration": 85052675,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.connectPointsSave()"
});
formatter.result({
  "duration": 95131738,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "EditedPoint",
      "offset": 16
    },
    {
      "val": "NewPoint2",
      "offset": 32
    }
  ],
  "location": "WebsiteStepDefs.checkpointLinked(String,String)"
});
formatter.result({
  "duration": 2039809818,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6914334224,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34762170,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 75717031,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20927517,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 81865199,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 547672999,
  "status": "passed"
});
formatter.scenario({
  "line": 49,
  "name": "Delete Point",
  "description": "",
  "id": "templates-tests;delete-point",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 48,
      "name": "@Admin"
    },
    {
      "line": 48,
      "name": "@SmokeTest"
    },
    {
      "line": 48,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 50,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "Click add new point button",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "Fill point mandatory fields with name Newone",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "Open Point with name Newone and click Delete",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "Check if point with name Newone is deleted from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1686301972,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.addNewPointButton()"
});
formatter.result({
  "duration": 2223806865,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Newone",
      "offset": 38
    }
  ],
  "location": "WebsiteStepDefs.fillPointFields(String)"
});
formatter.result({
  "duration": 1274133543,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Newone",
      "offset": 21
    }
  ],
  "location": "WebsiteStepDefs.DeletePointName(String)"
});
formatter.result({
  "duration": 3261078482,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Newone",
      "offset": 25
    }
  ],
  "location": "WebsiteStepDefs.checkpointIsDeleted(String)"
});
formatter.result({
  "duration": 1013657958,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Templates tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Templates page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8029718233,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32985941,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71924339,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 22915689,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToTemplates()"
});
formatter.result({
  "duration": 91646523,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtTemplatesPage()"
});
formatter.result({
  "duration": 535513011,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Delete Section",
  "description": "",
  "id": "templates-tests;delete-section",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@Admin"
    },
    {
      "line": 56,
      "name": "@SmokeTest"
    },
    {
      "line": 56,
      "name": "@Template"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "User go to CHILLER template",
  "keyword": "When "
});
formatter.step({
  "line": 59,
  "name": "Click on Delete Button, then click delete Section",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "Click on Delete Button, then click delete Section",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "Check if template with AutoSection name Disappears from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHILLER",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.goToOneTemplate(String)"
});
formatter.result({
  "duration": 1682970265,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.changeDeletedSectionName()"
});
formatter.result({
  "duration": 4352947615,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.changeDeletedSectionName()"
});
formatter.result({
  "duration": 4324077612,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoSection",
      "offset": 23
    }
  ],
  "location": "WebsiteStepDefs.checkTemplateIsDeleted(String)"
});
formatter.result({
  "duration": 1014734364,
  "status": "passed"
});
formatter.uri("Units.feature");
formatter.feature({
  "line": 1,
  "name": "Units Tests",
  "description": "",
  "id": "units-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Units tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Units page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8020173431,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34467678,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 65851765,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 21031401,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToUnits()"
});
formatter.result({
  "duration": 152703394,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtUnitsPage()"
});
formatter.result({
  "duration": 1046335029,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Add New Unit Category",
  "description": "",
  "id": "units-tests;add-new-unit-category",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Admin"
    },
    {
      "line": 11,
      "name": "@SmokeTest"
    },
    {
      "line": 11,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "User Click Add Category Button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user add AAuto Test as Category Name and click Add",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Check if category with name AAuto Test appears on list",
  "keyword": "Then "
});
formatter.match({
  "location": "UnitsStepDefs.addCategoryButton()"
});
formatter.result({
  "duration": 187962261,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AAuto Test",
      "offset": 9
    }
  ],
  "location": "UnitsStepDefs.addCategory(String)"
});
formatter.result({
  "duration": 2242637688,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AAuto Test",
      "offset": 28
    }
  ],
  "location": "UnitsStepDefs.CheckCategory(String)"
});
formatter.result({
  "duration": 533253071,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Units tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Units page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8460553312,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 31582580,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 65154258,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20374706,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToUnits()"
});
formatter.result({
  "duration": 148127399,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtUnitsPage()"
});
formatter.result({
  "duration": 1042889236,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Add new Unit",
  "description": "",
  "id": "units-tests;add-new-unit",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@Admin"
    },
    {
      "line": 17,
      "name": "@SmokeTest"
    },
    {
      "line": 17,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "User Click Add Category Button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "user add AAuto Test as Category Name and click Add",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Check if category with name AAuto Test appears on list",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Click category options button",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "Click Add new Unit",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User fill mandatory fields and click add",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "Check if unit with AutoUnit name appears on list",
  "keyword": "Then "
});
formatter.match({
  "location": "UnitsStepDefs.addCategoryButton()"
});
formatter.result({
  "duration": 177296068,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AAuto Test",
      "offset": 9
    }
  ],
  "location": "UnitsStepDefs.addCategory(String)"
});
formatter.result({
  "duration": 2244299367,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AAuto Test",
      "offset": 28
    }
  ],
  "location": "UnitsStepDefs.CheckCategory(String)"
});
formatter.result({
  "duration": 532651100,
  "status": "passed"
});
formatter.match({
  "location": "UnitsStepDefs.categoryOptions()"
});
formatter.result({
  "duration": 1112466447,
  "status": "passed"
});
formatter.match({
  "location": "UnitsStepDefs.addNewUnit()"
});
formatter.result({
  "duration": 1092816609,
  "status": "passed"
});
formatter.match({
  "location": "UnitsStepDefs.fillMandatory()"
});
formatter.result({
  "duration": 5594564086,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoUnit",
      "offset": 19
    }
  ],
  "location": "UnitsStepDefs.checkUnitsAdded(String)"
});
formatter.result({
  "duration": 27317772,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Units tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Units page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 8040280078,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34307215,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 68949269,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 18985258,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToUnits()"
});
formatter.result({
  "duration": 151305135,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtUnitsPage()"
});
formatter.result({
  "duration": 2061947769,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "Edit Unit",
  "description": "",
  "id": "units-tests;edit-unit",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 27,
      "name": "@Admin"
    },
    {
      "line": 27,
      "name": "@SmokeTest"
    },
    {
      "line": 27,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 29,
  "name": "User click unit with name AutoUnit",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Change symbol to ~!~",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "Check if Unit symbol is changed to ~!~",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AutoUnit",
      "offset": 26
    }
  ],
  "location": "UnitsStepDefs.clickUnit(String)"
});
formatter.result({
  "duration": 1141492274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "~!~",
      "offset": 17
    }
  ],
  "location": "UnitsStepDefs.changeSymbol(String)"
});
formatter.result({
  "duration": 4276126817,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "~!~",
      "offset": 35
    }
  ],
  "location": "UnitsStepDefs.checkSymbol(String)"
});
formatter.result({
  "duration": 2023430007,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click Units tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at Units page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7952513436,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34258983,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71931759,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20025489,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToUnits()"
});
formatter.result({
  "duration": 141944448,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.userAtUnitsPage()"
});
formatter.result({
  "duration": 1046069290,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Delete Unit",
  "description": "",
  "id": "units-tests;delete-unit",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 33,
      "name": "@Admin"
    },
    {
      "line": 33,
      "name": "@SmokeTest"
    },
    {
      "line": 33,
      "name": "@Unit"
    }
  ]
});
formatter.step({
  "line": 35,
  "name": "User click unit with name AutoUnit",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "user Click delete Unit button",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "User click Yes button to delete Unit",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "Check if Unit with name AutoUnit disappears from list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AutoUnit",
      "offset": 26
    }
  ],
  "location": "UnitsStepDefs.clickUnit(String)"
});
formatter.result({
  "duration": 1121752931,
  "status": "passed"
});
formatter.match({
  "location": "UnitsStepDefs.deleteButton()"
});
formatter.result({
  "duration": 1130087300,
  "status": "passed"
});
formatter.match({
  "location": "UnitsStepDefs.yesButton()"
});
formatter.result({
  "duration": 1096886171,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AutoUnit",
      "offset": 24
    }
  ],
  "location": "UnitsStepDefs.checkUnitIsDeleted(String)"
});
formatter.result({
  "duration": 2018669432,
  "status": "passed"
});
formatter.uri("UserPanel.feature");
formatter.feature({
  "line": 1,
  "name": "User Panel Tests",
  "description": "",
  "id": "user-panel-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click User Panel tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at User panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6458974646,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34560432,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 64173853,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 19623402,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickUserPanel()"
});
formatter.result({
  "duration": 113159315,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkUserPanel()"
});
formatter.result({
  "duration": 32060724,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Add New User",
  "description": "",
  "id": "user-panel-tests;add-new-user",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Admin"
    },
    {
      "line": 11,
      "name": "@SmokeTest"
    },
    {
      "line": 11,
      "name": "@UserPanel"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "User clicks on Add new User",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "User Fill Edonis as First Name",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "User Fill Sopi as Last Name",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "User Fill edon@kodelabs.co as Email",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "User Fill 123-456-7889 as Phone number",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "User Fill IT as Position",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User Fill Manager as Role",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "User click Save changes",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Check if User with name Edonis appear on the list",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.clickNewUser()"
});
formatter.result({
  "duration": 150809831,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_FirstName(String)"
});
formatter.result({
  "duration": 1123096001,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sopi",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_LastName(String)"
});
formatter.result({
  "duration": 99985531,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "edon@kodelabs.co",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_Email(String)"
});
formatter.result({
  "duration": 172031377,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123-456-7889",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_Phone(String)"
});
formatter.result({
  "duration": 149117079,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IT",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_Position(String)"
});
formatter.result({
  "duration": 161380952,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Manager",
      "offset": 10
    }
  ],
  "location": "WebsiteStepDefs.U_Role(String)"
});
formatter.result({
  "duration": 152825829,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.SaveButton()"
});
formatter.result({
  "duration": 1064875534,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 24
    }
  ],
  "location": "WebsiteStepDefs.checkUserIsAdded(String)"
});
formatter.result({
  "duration": 2567648865,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click User Panel tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at User panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7525190006,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 34452838,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 67568169,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 17470593,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickUserPanel()"
});
formatter.result({
  "duration": 102059964,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkUserPanel()"
});
formatter.result({
  "duration": 21827226,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Edit User",
  "description": "",
  "id": "user-panel-tests;edit-user",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 23,
      "name": "@Admin"
    },
    {
      "line": 23,
      "name": "@SmokeTest"
    },
    {
      "line": 23,
      "name": "@UserPanel"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "User click Edonis User",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "Click on Edit Button, then change some values for user and click save",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "Check if All Changed fields for Edonis are saved successful",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.openUser(String)"
});
formatter.result({
  "duration": 1123341335,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.editUser()"
});
formatter.result({
  "duration": 2412494922,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 32
    }
  ],
  "location": "WebsiteStepDefs.checkEditedUser(String)"
});
formatter.result({
  "duration": 531791738,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click User Panel tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at User panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 6541720077,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35605300,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71250948,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 19284852,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickUserPanel()"
});
formatter.result({
  "duration": 143252273,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkUserPanel()"
});
formatter.result({
  "duration": 27137830,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Assign Buildings to User",
  "description": "",
  "id": "user-panel-tests;assign-buildings-to-user",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@Admin"
    },
    {
      "line": 29,
      "name": "@SmokeTest"
    },
    {
      "line": 29,
      "name": "@UserPanel"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "User click Edonis User",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User go to buildings tab",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "User click assign buildings",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "User assign Chrysler House Building",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Check if Chrysler House is assigned to that user",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.openUser(String)"
});
formatter.result({
  "duration": 1124814261,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToBuildingsTab()"
});
formatter.result({
  "duration": 1114667024,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.ClickAssignBuilding()"
});
formatter.result({
  "duration": 2199813843,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 12
    }
  ],
  "location": "WebsiteStepDefs.AssignBuilding(String)"
});
formatter.result({
  "duration": 3282076028,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.checkAssignedBuilding(String)"
});
formatter.result({
  "duration": 19872446,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click User Panel tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at User panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7582719923,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 33258173,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 71918311,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 25327280,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickUserPanel()"
});
formatter.result({
  "duration": 99327444,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkUserPanel()"
});
formatter.result({
  "duration": 26200555,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Remove Building from User",
  "description": "",
  "id": "user-panel-tests;remove-building-from-user",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 37,
      "name": "@Admin"
    },
    {
      "line": 37,
      "name": "@SmokeTest"
    },
    {
      "line": 37,
      "name": "@UserPanel"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "User click Edonis User",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "User go to buildings tab",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "User clicks delete assigned buildings",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "User remove Chrysler House Building",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Check if Chrysler House is removed from user",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.openUser(String)"
});
formatter.result({
  "duration": 1151195222,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.goToBuildingsTab()"
});
formatter.result({
  "duration": 1136447874,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.ClickRemoveBuilding()"
});
formatter.result({
  "duration": 2720078173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 12
    }
  ],
  "location": "WebsiteStepDefs.removedBuilding(String)"
});
formatter.result({
  "duration": 3227882919,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrysler House",
      "offset": 9
    }
  ],
  "location": "WebsiteStepDefs.checkRemovedBuilding(String)"
});
formatter.result({
  "duration": 1032448897,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User Go To admin panel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Check if user is at Admin panel",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click User Panel tab",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Check if user is at User panel",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7625591999,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 35996720,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickAdminPanel()"
});
formatter.result({
  "duration": 67547764,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkDevicesButton()"
});
formatter.result({
  "duration": 20834763,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.clickUserPanel()"
});
formatter.result({
  "duration": 114488473,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.checkUserPanel()"
});
formatter.result({
  "duration": 24530992,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "Delete Users",
  "description": "",
  "id": "user-panel-tests;delete-users",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 45,
      "name": "@Admin"
    },
    {
      "line": 45,
      "name": "@SmokeTest"
    },
    {
      "line": 45,
      "name": "@UserPanel"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "User click Edonis User",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "User click on Delete user button",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "Check if User Edonis dissapears from the list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 11
    }
  ],
  "location": "WebsiteStepDefs.openUser(String)"
});
formatter.result({
  "duration": 1125592463,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.removedUser()"
});
formatter.result({
  "duration": 2143922881,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edonis",
      "offset": 14
    }
  ],
  "location": "WebsiteStepDefs.checkRemovedUser(String)"
});
formatter.result({
  "duration": 1036597763,
  "status": "passed"
});
formatter.uri("VirtualDevice.feature");
formatter.feature({
  "line": 1,
  "name": "Virtual Devices Tests",
  "description": "Create Virtual Device, add points to virtual device, remove points from Virtual device",
  "id": "virtual-devices-tests",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User goes to Virtual Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Check if user is at Virtual Device page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 9795680654,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 30914754,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2164522978,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1930206210,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1652028149,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1022120327,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 115088589,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.goToFilters()"
});
formatter.result({
  "duration": 88089426,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.CheckType()"
});
formatter.result({
  "duration": 1022126820,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Create Virtual Device",
  "description": "",
  "id": "virtual-devices-tests;create-virtual-device",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 15,
      "name": "@SmokeTest"
    },
    {
      "line": 15,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "User select ExactLogic Device",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "User Click Add Virtual Device and add VirtualDevice as name",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "User save the changes",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "Check if Virtual Device with name VirtualDevice appears on list",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ExactLogic",
      "offset": 12
    }
  ],
  "location": "VirtualDeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 72836571,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "VirtualDevice",
      "offset": 38
    }
  ],
  "location": "VirtualDeviceStepDefs.SearchConnectorfield(String)"
});
formatter.result({
  "duration": 2418314279,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.filterConnector()"
});
formatter.result({
  "duration": 2064399708,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "VirtualDevice",
      "offset": 34
    }
  ],
  "location": "VirtualDeviceStepDefs.CheckDeviceisAppeared(String)"
});
formatter.result({
  "duration": 22755689,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User goes to Virtual Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Check if user is at Virtual Device page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7673124014,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 36850053,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2254673387,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1899718122,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1668530858,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1019089142,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 134595585,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.goToFilters()"
});
formatter.result({
  "duration": 85525254,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.CheckType()"
});
formatter.result({
  "duration": 1021280445,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Add Points to Virtual Device",
  "description": "",
  "id": "virtual-devices-tests;add-points-to-virtual-device",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@SmokeTest"
    },
    {
      "line": 22,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "User select ExactLogic Device",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "User also select VirtualDevice Device",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "User check all points and move to Virtual device",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "User save the changes",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Check if points are moved to Virtual Device",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ExactLogic",
      "offset": 12
    }
  ],
  "location": "VirtualDeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 86230181,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "VirtualDevice",
      "offset": 17
    }
  ],
  "location": "VirtualDeviceStepDefs.filterArea(String)"
});
formatter.result({
  "duration": 1117306326,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.CheckArea()"
});
formatter.result({
  "duration": 2122766263,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.filterConnector()"
});
formatter.result({
  "duration": 2064644578,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 999838609,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Steps That execute before every scenario",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "Open KodeLabs Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Check if user is logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User click twice on Automation Testing building",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Check if Automation Testing Dashboard appears",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User Click on Devices button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Check if the list of Devices appears",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User click on threedots button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User goes to Virtual Device Page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Check if user is at Virtual Device page",
  "keyword": "Then "
});
formatter.match({
  "location": "WebsiteStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 7808452816,
  "status": "passed"
});
formatter.match({
  "location": "WebsiteStepDefs.theAssertionThatTheTestHasPassedAndWorkedFine()"
});
formatter.result({
  "duration": 32933535,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 20
    }
  ],
  "location": "SitesStepDefs.openAreaTab(String)"
});
formatter.result({
  "duration": 2182880758,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Testing",
      "offset": 9
    }
  ],
  "location": "SitesStepDefs.openMenuTab(String)"
});
formatter.result({
  "duration": 1836039573,
  "status": "passed"
});
formatter.match({
  "location": "DashboardStepDefs.clickSystems()"
});
formatter.result({
  "duration": 1664663036,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Devices",
      "offset": 21
    }
  ],
  "location": "DashboardStepDefs.openMensuTab(String)"
});
formatter.result({
  "duration": 1023374355,
  "status": "passed"
});
formatter.match({
  "location": "DeviceStepDefs.openThreedot()"
});
formatter.result({
  "duration": 103681759,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.goToFilters()"
});
formatter.result({
  "duration": 95431796,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.CheckType()"
});
formatter.result({
  "duration": 1019426301,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Remove Points to Virtual Device",
  "description": "",
  "id": "virtual-devices-tests;remove-points-to-virtual-device",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 30,
      "name": "@SmokeTest"
    },
    {
      "line": 30,
      "name": "@VirtualDevice"
    }
  ]
});
formatter.step({
  "line": 32,
  "name": "User select ExactLogic Device",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "User also select VirtualDevice Device",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "User check all points and move to real device",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "User save the changes",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Check if points are moved to Virtual Device",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ExactLogic",
      "offset": 12
    }
  ],
  "location": "VirtualDeviceStepDefs.Searchfield(String)"
});
formatter.result({
  "duration": 72877845,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "VirtualDevice",
      "offset": 17
    }
  ],
  "location": "VirtualDeviceStepDefs.filterArea(String)"
});
formatter.result({
  "duration": 1112820301,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.CheckAreas()"
});
formatter.result({
  "duration": 2132638022,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.filterConnector()"
});
formatter.result({
  "duration": 2063800057,
  "status": "passed"
});
formatter.match({
  "location": "VirtualDeviceStepDefs.clickCheckbox()"
});
formatter.result({
  "duration": 999709217,
  "status": "passed"
});
});