package website;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import selenium.WebDriverFactory;
import stepdefs.CommonStepObjects;

@RunWith(Cucumber.class)

@CucumberOptions(monochrome = true,

        features = "src/test/java/website",
        format = {"pretty", "html:cucumber-html-reports",
                "json: cucumber-html-reports/cucumber.json"},
        tags = {"@SmokeTest"},
        glue = "stepdefs")


public class WebsiteTestsInit {

    @BeforeClass
    public static void InitilizeTests() {
        CommonStepObjects.setWebDriver(WebDriverFactory.getInstance().getWebDriver());
    }

    @AfterClass
    public static void TearDown() {
        CommonStepObjects.closeDriver();
    }


}